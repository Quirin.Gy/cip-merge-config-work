# How to Merged
kconfig_merge.sh <kernel_dir> <arch> <kernel_version> [kconfig_files]
  kernel_dir: path to the kernel source directory
  arch: architecture of the kernel
  kernel_version: version of the kernel

```bash
kconfig_merge.sh ~/02_repos/linux x86 5.10
```
# Manual intervention

- Use CONFIG_GENERIC_CPU instead of CONFIG_MATOM
- Add missing CONFIG_EFI_PARTITIONS
