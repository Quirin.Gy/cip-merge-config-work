cat ipc227e-merged.log  | sed -e "s/^.\{15\}//" > ipc227e-merged.log.strip
cat ipc227e-NO-merged.log  | sed -e "s/^.\{15\}//" > ipc227e-NO-merged.log.strip
cat ipc227e-NO-merged-localbuild.log | sed -e "s/^.\{15\}//" > ipc227e-NO-merged-localbuild.log.strip
diff -urN ipc227e-NO-merged.log.strip ipc227e-merged.log.strip > ipc227e.log.diff
