#!/usr/bin/env bash
ARCH=$1
cip_kernel_config_dir=$2
for f in merged_defconfig_*"${ARCH}"; do
	kversion=$(awk -v var="$f" 'BEGIN{split(var,a,"_"); print a[3]}')
	kversion_cip=$(awk -v var="$f" 'BEGIN{split(var,a,"_"); print a[4]}')
	dest="$(readlink -f "$cip_kernel_config_dir"/"$kversion"*"${kversion_cip}"/"${ARCH}")"
	if [ -d "$dest" ]; then
		cp "$(readlink -f "$f")" "$dest"/cip_merged_defconfig
	else
		echo "$dest does not exist"
	fi
done
